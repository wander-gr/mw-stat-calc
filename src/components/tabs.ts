import type { TemplateResult } from 'lit';
import { LitElement, css, html } from 'lit';
import { classMap } from 'lit/directives/class-map.js';
import { customElement, property } from 'lit/decorators.js';

export type TabItem = {key: number, value: unknown};

@customElement('mw-tabs')
export class TabsElement extends LitElement{
	public static styles = css`
		.container{
			display: flex;
			gap: 4px;
			padding-top: 1em;
		}
		
		.tab{
			cursor: pointer;
			padding: 6px;
			position: relative;
		}
		
		.inner-card{padding: .1em .5em;}
		.active{top: -1em;}
		.active .tab-name{font-weight: bold;}
	`;
	
	@property({type: Number})
	public active: number = 0;
	
	@property({type: Array})
	public items: TabItem[] = [];
	
	protected render(): TemplateResult{
		return html`<div class="container">
			${this.items.map(item => this.renderTab(item))}
		</div>`;
	}
	
	private renderTab(item: TabItem): TemplateResult{
		let active = item.key === this.active;
		
		return html`<wired-card
			class=${classMap({tab: true, active})}
			@click=${() => this.select(item)}
		>
			<wired-card class="inner-card" fill=${active ? '#E6E6E6' : false}>
				<span class="tab-name">${item.value}</span>
			</wired-card>
		</wired-card>`;
	}
	
	private select(item: TabItem): void{
		this.active = item.key;
		this.dispatchEvent(new CustomEvent('select', {detail: item}));
	}
}

declare global{
	interface HTMLElementTagNameMap{
		'mw-tabs': TabsElement;
	}
}

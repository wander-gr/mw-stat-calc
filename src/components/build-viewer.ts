import type { TemplateResult } from 'lit';
import { LitElement, css, html } from 'lit';
import { customElement } from 'lit/decorators.js';

export type TabItem = {key: number, value: unknown};

@customElement('mw-build-viewer')
export class BuildViewerElement extends LitElement{
	public static styles = css`
		wired-item{
			--wired-item-selected-color: #000;
			--wired-item-selected-bg: #E6E6E6;
		}
		
		.container{
			display: flex;
			gap: 4px;
		}
	`;
	
	protected render(): TemplateResult{
		return html`<div class="container">
			<wired-listbox selected="1">
				<wired-item value="1">Hi there</wired-item>
				<wired-item value="2">Hi there</wired-item>
			</wired-listbox>
			<div>
				<wired-card style="width: 300px; position: relative; margin-top: 1em; padding-top: 1em;">
					<div style="position: absolute;top: -1.5em; background: white; padding: 0 .5em;">Lvl: 10</div>
					Test card
				</wired-card>
			</div>
		</div>`;
	}
	
	private renderItem(): TemplateResult{
		return html``;
	}
}

declare global{
	interface HTMLElementTagNameMap{
		'mw-build-viewer': BuildViewerElement;
	}
}

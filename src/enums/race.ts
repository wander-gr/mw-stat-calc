export enum CharacterRace{
	Human = 0,
	Centaur = 1,
	Mage = 2,
	Borg = 3,
}

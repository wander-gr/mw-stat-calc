import type { TemplateResult } from 'lit';
import { LitElement, css, html } from 'lit';
import { customElement } from 'lit/decorators.js';
import 'wired-elements';
import './components/build-viewer';
import './components/tabs';
import type { TabItem } from './components/tabs';
import { CharacterRace } from './enums/race';

@customElement('mw-app')
export class App extends LitElement{
	public static styles = css`
		wired-card{background-color: #FFF; border-radius: 8px;}
		wired-divider{padding: 4px 0;}
		
		.container{
			display: flex;
			gap: 1em;
			width: 100%;
		}
	`;
	
	private tabs: TabItem[] = [
		{key: CharacterRace.Human, value: 'Human'},
		{key: CharacterRace.Mage, value: 'Mage'},
		{key: CharacterRace.Borg, value: 'Borg'},
		{key: CharacterRace.Centaur, value: 'Centaur'},
	];
	
	private stats: TabItem[] = [
		{key: 1, value: 'STA'},
		{key: 2, value: 'INT'},
		{key: 3, value: 'STR'},
		{key: 4, value: 'AGI'},
	];
	
	protected render(): TemplateResult{
		return html`<div class="container">
			<wired-card>
				<mw-tabs .items=${this.tabs} active=${CharacterRace.Borg} @select=${console.log}></mw-tabs>
				<wired-divider></wired-divider>
				<mw-build-viewer></mw-build-viewer>
			</wired-card>
			<wired-card style="display: flex; flex-direction: column;">
				<mw-tabs .items=${this.stats} active=${1} @select=${console.log}></mw-tabs>
				<wired-divider></wired-divider>
				<wired-combo>
					<wired-item>Test</wired-item>
					<wired-item>Test</wired-item>
					<wired-item>Test</wired-item>
				</wired-combo>
				<div>hp = sta * a</div>
				a: <wired-input type="number" step="0.01"></wired-input>
			</wired-card>
		</div>`;
	}
}
